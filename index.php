<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="form-group row">
			<div class="col-xs-3">
				<label>Search here</label>
				<input class="form-control" id="search-text" type="text">
				<br>
				<button type="button" class="btn btn-primary" onclick="searchfunction()">Search</button>
			</div>
		</div>
		<div id="alldata"></div>
  	</div>
</body>
</html>
<script type="text/javascript">
	function searchfunction(){
		var search = document.getElementById("search-text").value;
		var encoded = encodeURI(search);
		$.get("http://www.omdbapi.com/?s="+encoded+"&apikey=cbf6580e",
        	function(data){
        		var str = '';
        		var j = 1;
        		for (var i = 0; i < data.Search.length; i++) {
        			str += '<h4 id="title">'+data.Search[i].Title+'</h4><a id="movieLink" href="#"><img id="poster" src="'+data.Search[i].Poster+'"><a>';
        			if (j++ == 3) break;
        		}
        		document.getElementById("alldata").innerHTML = str;
        	});
	}
</script>